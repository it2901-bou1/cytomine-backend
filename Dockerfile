FROM python:3

WORKDIR /usr/src

# Clone our github cytomine-backend repository
RUN git clone --recurse-submodules https://gitlab.stud.idi.ntnu.no/it2901-bou1/cytomine-backend.git

WORKDIR cytomine-backend/Cytomine-python-client

# Install essential python packages for the cytomine client
RUN pip install requests \
requests-toolbelt \
cachecontrol \
six \
future \
shapely \
numpy \
opencv-python-headless

# Build and install Cytomine-python-client
RUN python setup.py build
RUN python setup.py install

# Navigate back to cytomine-backend repo
WORKDIR ../

RUN pip install --no-cache-dir -r requirements.txt

RUN export FLASK_ENV=development

RUN export FLASK_APP=app.py

CMD [ "flask", "init-db" ]

CMD [ "flask", "run", "--port", "80" ]