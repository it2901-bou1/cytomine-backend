# Cytomine-backend
The project is an API to store and handle the rating of images made in the Cytomine.

## Usage

The app functions as a REST API. You communicate through HTTP requests to the following endpoints

| Endpoint                         	| Methods   	| Rule                                	|
|----------------------------------	|-----------	|-------------------------------------	|
| filtering.elo_slider             	| GET       	| /filter/elo-slider                  	|
| get_images.get_images            	| GET       	| /get_images/project-id                        	|
| get_images.get_n_images          	| GET       	| /get_images/propject-id/n                       	|
| get_images.get_two_elo_images    	| GET       	| /get_two_elo_images/project-id                	|
| get_images.get_two_random_images 	| GET       	| /get_two_random_images/project-id             	|
| hello                            	| GET       	| /hello                              	|
| props.add_absolute_rating        	| GET, POST 	| /props/absolute                     	|
| props.add_properties             	| GET, POST 	| /props/properties                   	|
| props.elo                        	| GET, POST 	| /props/elo                          	|
| props.elo_all                    	| GET       	| /props/elo-all                      	|
| sorting.sorting_category         	| GET       	| /sort/category                      	|
| sorting.sorting_elo              	| GET       	| /sort/elo                           	|
| static                           	| GET       	| /static/                            	|


## Installation
To install and run this project you have to do the following:

1. Clone this project
2. Clone the submodule Cytomine-python-client
3. Install the submodules requirements
4. Build and install the submodule
5. Setup a virtualenv for the project
6. Install the requirements located in requirements.txt
7. Set the flask enviroment to development
8. Initialize the database
9. Run the app 

For powershell it would look like this, after cloning the repo:

```bash
>cd Cytomine-python-client 
>git submodule init 
>git submodule update 
>python -m pip install requests requests-toolbelt cachecontrol six future shapely numpy opencv-python-headless 
>python setup.py build 
>python setup.py install
>cd .. 
>python -m pip install virtualenv 
>python -m venv env
>/env/Scripts/actvate 
>python -m pip install -r requirements.txt 
>$env:FLASK_ENV = "development" 
>flask init-db 
>flask run
```

## Files written by bachelor team
All code written by us except for external cytomine-python-client 
Full changelog can be found at https://gitlab.stud.idi.ntnu.no/it2901-bou1/cytomine-backend
