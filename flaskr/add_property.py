from flask import (Blueprint, request)
from flaskr.db import get_db
import json
import numpy as np
import sys

sys.path.append("Cytomine-python-client")

from cytomine.models import (
    Property,
    ImageInstance,
    ImageInstanceCollection,
)
from cytomine import Cytomine


bp = Blueprint("props", __name__, url_prefix="/props")
# This is an endpoint for adding elo properties to an image.
# ----------------------------------------------------------
# PARAMS:
#   Accessed by a url to /props/properties
#   int image, refferences the imageid in Cytomine
#   String category, is the category of the ranking
#   String Score, is the scoring related to the category.
# RETURNS:
#    Adds the rating to the database and the category answer
@bp.route("/properties", methods=("GET", "POST"))
def add_properties():
    # Method for adding a new rating
    if request.method == "POST":
        imageid = request.args.get("image")
        # category = request.args.get("category")
        # score = request.args.get("score")
        jsondata = request.json
        category = jsondata["category"]
        score = jsondata["score"]

        db = get_db()
        error = None

        if not imageid:
            error = "You need to specify the image id"
        elif not category:
            error = "You need to specify the category"
        elif not score:
            error = "You need to specify the Score"

        if error is None:
            imageid = int(imageid)

            with Cytomine(
                host="core.awilson.website",
                public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
                private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
            ) as cytomine:
                props = Property(
                    ImageInstance().fetch(imageid), key=category, value=score
                ).save()

            db.execute(
                "INSERT INTO rating (imageid, category, score) VALUES (?, ?, ?)",
                (imageid, category, score),
            )
            db.commit()
            return {"category": category, "score": score}

        return error, 400

    # Method for getting the ratings in a huma readable format
    if request.method == "GET":
        imageid = int(request.args.get("image"))
        db = get_db()
        rows = db.execute("SELECT * FROM rating WHERE imageid=?", (imageid,)).fetchall()
        ratings = {}
        for row in rows:
            rating = {"category": row["category"], "score": row["score"]}
            ratings[row["ratingid"]] = rating
        json_ratings = json.dumps(ratings)
        return json_ratings


# Updates the elos of two images.
# --------------------------------------------
# PARAMS:
#   int winner, the id of the winner image. Defined by the query winner
#   int loser, the id of the loser image. Defined by the querying loser
# RETURNS:
#   Returns the new elos for both images.
@bp.route("/elo", methods=(["GET", "POST"]))
def elo():
    if request.method == "POST":
        error = None
        winner_id = request.args.get("winner")
        loser_id = request.args.get("loser")
        db = get_db()

        if not winner_id:
            error = "You need to specify the winner id"
        elif not loser_id:
            error = "You need to specify the loser id"
        elif winner_id == loser_id:
            error = "The winner and loser is the same"

        # the previous elo of the images

        winner_elo = db.execute(
            'SELECT score FROM rating WHERE imageid=? AND category="elo"', (winner_id,)
        ).fetchone()
        loser_elo = db.execute(
            'SELECT score FROM rating WHERE imageid=? AND category="elo"', (loser_id,)
        ).fetchone()

        if not winner_elo:
            # error = "The winner has no elo"
            db.execute(
                "INSERT INTO rating (imageid, category, score) VALUES (?, ?, ?)",
                (winner_id, "elo", 1000),
            )
            winner_elo = db.execute(
                'SELECT score FROM rating WHERE imageid=? AND category="elo"',
                (winner_id,),
            ).fetchone()
        if not loser_elo:
            db.execute(
                "INSERT INTO rating (imageid, category, score) VALUES (?, ?, ?)",
                (loser_id, "elo", 1000),
            )
            loser_elo = db.execute(
                'SELECT score FROM rating WHERE imageid=? AND category="elo"',
                (loser_id,),
            ).fetchone()

        if error == None:

            expected_winner = expected_elo(winner_elo[0], loser_elo[0])
            expected_loser = expected_elo(loser_elo[0], winner_elo[0])

            new_winner = round(winner_elo[0] + 32 * (1 - expected_winner))
            new_loser = round(loser_elo[0] + 32 * (0 - expected_loser))

            with Cytomine(
                host="core.awilson.website",
                public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
                private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
            ) as cytomine:
                # Add ELO to cytomine model
                winner_props = Property(ImageInstance().fetch(int(winner_id))).fetch(
                    id=int(winner_id), key="elo"
                )
                # Need to save the winner props if they dont already exist
                if not winner_props:
                    Property(
                        ImageInstance().fetch(int(winner_id)),
                        key="elo",
                        value=new_winner,
                    ).save()
                else:
                    winner_props.update(value=new_winner)
                loser_props = Property(ImageInstance().fetch(int(loser_id))).fetch(
                    id=int(loser_id), key="elo"
                )
                if not loser_props:
                    Property(
                        ImageInstance().fetch(int(loser_id)),
                        key="elo",
                        value=new_loser,
                    ).save()
                else:
                    loser_props.update(value=new_loser)

            db.execute(
                'UPDATE rating SET score=? WHERE imageid=? AND category="elo"',
                (new_winner, winner_id),
            )
            db.execute(
                'UPDATE rating SET score=? WHERE imageid=? AND category="elo"',
                (new_loser, loser_id),
            )

            db.commit()

            return {"winner": new_winner, "loser": new_loser}

        return error, 400

    if request.method == "GET":
        db = get_db()
        imageid = request.args.get("image")
        row = db.execute(
            'SELECT score FROM rating WHERE imageid=? AND category="elo"', (imageid,)
        ).fetchone()
        return {"elo": row[0]}


# Simple helper function for changing elo
def expected_elo(elo_A, elo_B):
    return np.power(10, (elo_A / 400)) / (
        np.power(10, (elo_B / 400)) + np.power(10, (elo_A / 400))
    )


# Adds a base elo for every image in a project
# ---------------------------------------------------------------------
# PARAMS:
#    int project, the id of the project
# RETURNS:
#   if every image has an elo, returns success. Returns an error otherwise.
@bp.route("/elo-all", methods=(["GET"]))
def elo_all():
    if request.method == "GET":
        project_id = request.args.get("project")
        db = get_db()

        if not project_id:
            return "You need to specify the project id", 404

        with Cytomine(
            host="http://core.awilson.website/",
            public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
            private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
        ) as cytomine:
            images = ImageInstanceCollection().fetch_with_filter("project", project_id)

            for image in images:
                score = db.execute(
                    'SELECT score FROM rating WHERE imageid=? AND category="elo"',
                    (image.id,),
                ).fetchone()
                if score == None:
                    db.execute(
                        'INSERT INTO rating (imageid, category, score) VALUES (?, "elo", 1000)',
                        (image.id,),
                    )
                    prop = Property(image, key="elo", value=1000).save()
                    print(prop)

        db.commit()

        return "success"


# This is an endpoint for adding aboslute rating properties to an image.
# ----------------------------------------------------------
# PARAMS:
#   Accessed by a url to /props/absolute
#   int image, refferences the imageid in Cytomine
#   String category, is the category of the ranking
#   String Score, is the scoring related to the category.
# RETURNS:
#    Adds the rating to the database and the category answer
@bp.route("/absolute", methods=("GET", "POST"))
def add_absolute_rating():
    # Method for adding a new rating
    if request.method == "POST":
        imageid = request.args.get("image")
        jsondata = request.json
        category = jsondata["category"]
        score = jsondata["score"]

        db = get_db()
        error = None

        if not imageid:
            error = "You need to specify the image id"
        elif not category:
            error = "You need to specify the category"
        elif not score and score != 0:
            error = "You need to specify the Score"
        elif type(score) != int:
            error = "The score needs to be an integer"

        # Check if we already have scores
        score = str(score)
        existingScore = db.execute(
            "SELECT score FROM absolute_rating WHERE imageid=?", (imageid,)
        ).fetchone()
        if existingScore:
            score = existingScore[0] + "," + score
        # Sqllite dont have arrays, so lets store it as a string with , between
        if error is None:
            imageid = int(imageid)

            with Cytomine(
                host="core.awilson.website",
                public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
                private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
            ) as cytomine:

                # Add absolute rating to cytopmine
                props = Property(ImageInstance().fetch(int(imageid))).fetch(
                    id=int(imageid), key=category
                )
                # Need to save they property if it dont already exist
                if not props:
                    Property(
                        ImageInstance().fetch(int(imageid)),
                        key=category,
                        value=score,
                    ).save()
                # If property exist, update
                else:
                    props.update(value=score)

            # Overwrite if exists, else create
            db.execute(
                "REPLACE     INTO absolute_rating (imageid, category, score) VALUES (?, ?, ?)",
                (imageid, category, score),
            )
            db.commit()
            return {"category": category, "score": score}
        return error, 400

    # Method for getting the ratings in a huma readable format
    if request.method == "GET":
        imageid = int(request.args.get("image"))
        db = get_db()
        score = db.execute(
            "SELECT score FROM absolute_rating WHERE imageid=?", (imageid,)
        ).fetchone()
        # Parse our strings x,y,z to int and return as array
        if score:
            score = [int(x) for x in score[0].split(",")]
        else:
            score = []
        ratings = {"category": "absolute_rating", "score": score}
        json_ratings = json.dumps(ratings)
        return json_ratings


# the python command
# python add_properties.py --cytomine_host='http://core.it2901-digipat.jenslien.net' --cytomine_public_key='ca560dc0-1285-42a6-8a19-8e40ad48daa6' --cytomine_private_key='eb27e341-ba4a-4ee3-b90f-964a6696d825' --cytomine_id_project='203' --cytomine_id_image_instance='2538' --key='testerino' --value='testerano'
