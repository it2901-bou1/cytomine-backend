import os
import sys
from flask import Flask
from flask_cors import CORS

# Fixing of relative imports
# https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
PACKAGE_PARENT = ".."
SCRIPT_DIR = os.path.dirname(
    os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)))
)
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE=os.path.join(app.instance_path, "flaskr.sqlite"),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile("config.py", silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route("/hello")
    def hello():
        return "hello world"

    # Imports
    from flaskr import db
    from flaskr import add_property
    from flaskr import get_images
    from flaskr import sorting
    from flaskr import filtering

    db.init_app(app)

    # Register Flask blueprints
    app.register_blueprint(add_property.bp)
    app.register_blueprint(get_images.bp)
    app.register_blueprint(sorting.bp)
    app.register_blueprint(filtering.bp)

    return app
