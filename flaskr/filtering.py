from flask import (Blueprint, request)
from cytomine import Cytomine
from cytomine.models.image import ImageInstanceCollection
from flaskr.db import get_db

bp = Blueprint('filtering', __name__, url_prefix=('/filter'))

# Filters images in a project 
# ----------------------------------------------------------
# PARAMS:
#   int upper, defines the upper elo limit
#   int lower, defines the lower elo limit 
#   int project, is the project_id 
# RETURNS:
#    returns a list of image ids that meet the requirements
@bp.route('/elo-slider', methods=['GET'])
def elo_slider():
    upper = int(request.args.get("upper"))
    lower = int(request.args.get("lower"))
    project_id = int(request.args.get("project"))
    db = get_db()

    if upper < lower:
        return "The upper limit is smaller than the lower limit", 404
    if not upper:
        return "No upper limit", 404
    if not lower:
        return "No lower limit", 404

    image_ids = []
    with Cytomine(
        host="http://core.awilson.website/",
        public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
        private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
    ) as cytomine:
        image_instance = ImageInstanceCollection().fetch_with_filter(
            "project", project_id
        )
        for image in image_instance:
            image_ids.append(image.id)

    matching_images = db.execute(
        'SELECT imageid FROM rating WHERE category="elo" AND score>=? AND score<=?', (lower, upper)).fetchall()

    matches = []
    for match in matching_images:
        if int(match[0]) in image_ids:
            matches.append(match[0])

    return {"matches": matches}
