from flask import Blueprint
from cytomine import Cytomine
from cytomine.models.image import ImageInstanceCollection
from flaskr.db import get_db

import random  # for returning random images to frontend

bp = Blueprint("get_images", __name__)


# Get all image ids of the corresponding project
# ------------------------------------------------------------------------
# PARAMS:
# 	int id_project, the id of the project
#
# RETURN:
# 	 id_images: list of all the image ids belonging to project
@bp.route("/get_images/<id_project>")
def get_images(id_project):
    id_images = []
    with Cytomine(
        host="http://core.awilson.website/",
        public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
        private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
    ) as cytomine:
        image_instances = ImageInstanceCollection().fetch_with_filter(
            "project", id_project
        )
        for image in image_instances:
            id_images.append(image.id)
    return {"id_images": id_images}


# Get n images for comparing in frontend
# ------------------------------------------------------------------------
# PARAMS:
# 	int id_project, the id of the project
#   int n, number of imageids to be returned
#
# RETURN:
# 	 id_images: returns n imageids to be compared
@bp.route("/get_images/<id_project>/<n>")
def get_n_images(id_project, n):
    id_images = []
    with Cytomine(
        host="http://core.awilson.website/",
        public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
        private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
    ) as cytomine:
        image_instances = ImageInstanceCollection().fetch_with_filter(
            "project", id_project
        )
        for image in image_instances:
            id_images.append(image.id)
        # get up to four random id_images from the list of all id_images
        ids_to_get = min(len(id_images), int(n))
        images = random.sample(id_images, ids_to_get)
    return {"id_images": images}


# Get two images for comparing in frontend
# ------------------------------------------------------------------------
# PARAMS:
# 	int id_project, the id of the project
#
# RETURN:
# 	 id_image1: int first imageid to be compared
# 	 id_image2: int second imageid to be compared
@bp.route("/get_two_random_images/<id_project>")
def get_two_random_images(id_project):
    id_images = []
    with Cytomine(
        host="http://core.awilson.website/",
        public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
        private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
    ) as cytomine:
        image_instances = ImageInstanceCollection().fetch_with_filter(
            "project", id_project
        )
        for image in image_instances:
            id_images.append(image.id)
        # get two random id_images from the list of all id_images
        id_image1, id_image2 = random.sample(id_images, 2)
    return {"id_image1": id_image1, "id_image2": id_image2}


# Get two images for comparing in frontend
# ------------------------------------------------------------------------
# PARAMS:
# 	int id_project, the id of the project
#
# RETURN:
#    returns images with either elo rating of 1000 or if none the two imageids with closest elo rating
# 	 id_image1: int first imageid to be compared
# 	 id_image2: int second imageid to be compared


@bp.route("/get_two_elo_images/<id_project>")
def get_two_elo_images(id_project):
    db = get_db()
    id_images = []
    with Cytomine(
        host="http://core.awilson.website/",
        public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
        private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
    ) as cytomine:
        image_instances = ImageInstanceCollection().fetch_with_filter(
            "project", id_project
        )
        for image in image_instances:
            id_images.append(image.id)

        scores = {}
        for image in id_images:
            score = db.execute(
                'SELECT score FROM rating WHERE imageid=? AND category="elo" ORDER BY score DESC',
                (image,),
            ).fetchone()
            scores[image] = int(score[0])

        # list of id images sorted based on elo rating
        sorted_id_images = sorted(scores, key=scores.get)

        # initialize id_images
        id_image1 = sorted_id_images[0]
        id_image2 = sorted_id_images[1]
        current_difference_score = id_image2 - id_image1

        for i in range(len(sorted_id_images) - 1):
            score1 = scores[sorted_id_images[i]]
            score2 = scores[sorted_id_images[i + 1]]

            # if one has elo rating of 1000 is probably a new image that should be rated
            if score1 == 1000:
                return {
                    "id_image1": sorted_id_images[i],
                    "is_image_2": sorted_id_images[i + 1],
                }

            # want the id for images with smalles difference in score
            if score2 - score1 < current_difference_score:
                id_image1 = sorted_id_images[i]
                id_image2 = sorted_id_images[i + 1]
                current_difference_score = score2 - score1

    return {"id_image1": id_image1, "id_image2": id_image2}
