DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS rating;
DROP TABLE IF EXISTS absolute_rating;

CREATE TABLE image (
    id INTEGER PRIMARY KEY,
    projectid INTEGER,
    title TEXT NOT NULL
);

CREATE TABLE rating (
    ratingid INTEGER PRIMARY KEY AUTOINCREMENT,
    imageid INTEGER NOT NULL,
    category TEXT NOT NULL,
    score INTEGER NOT NULL,
    FOREIGN KEY(imageid) REFERENCES image (id)
);

CREATE TABLE absolute_rating (
    imageid INTEGER PRIMARY KEY,
    category TEXT NOT NULL,
    score  TEXT NOT NULL,
    FOREIGN KEY (imageid) REFERENCES image (id)
)


