from flask import Blueprint, request
from cytomine import Cytomine
from cytomine.models.image import ImageInstanceCollection
from flaskr.db import get_db

bp = Blueprint("sorting", __name__, url_prefix=("/sort"))


@bp.route("/elo", methods=["GET"])
def sorting_elo():
    db = get_db()
    project_id = request.args.get("project")

    image_ids = []
    with Cytomine(
        host="http://core.awilson.website/",
        public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
        private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
    ) as cytomine:
        image_instance = ImageInstanceCollection().fetch_with_filter(
            "project", project_id
        )
        for image in image_instance:
            image_ids.append(image.id)

        scores = {}
        for image in image_ids:
            score = db.execute(
                'SELECT score FROM rating WHERE imageid=? AND category="elo" ORDER BY score DESC',
                (image,),
            ).fetchone()
            scores[image] = int(score[0])

        # sorted_dict = {}
        # sorted_keys =
        # for x in sorted_keys:
        #     sorted_dict[x] = scores[x]
        # print(sorted_keys)
        # print(sorted_dict)

        return {"sorted_ids": sorted(scores, key=scores.get)}


@bp.route("/category", methods=["GET"])
def sorting_category():
    db = get_db()
    project_id = request.args.get("project")
    category = request.args.get("category")
    descending = request.args.get("descending")

    image_ids = []
    with Cytomine(
        host="http://core.awilson.website/",
        public_key="6aabbd98-d7c0-464c-9fff-8bf52d4c67bb",
        private_key="28f84258-c3cb-42f8-84d3-ae82333c160f",
    ) as cytomine:
        image_instance = ImageInstanceCollection().fetch_with_filter(
            "project", project_id
        )
        for image in image_instance:
            image_ids.append(image.id)

        scores = {}
        for image in image_ids:
            score = db.execute(
                "SELECT score FROM rating WHERE imageid=? AND category=?",
                (image, category),
            ).fetchone()
            if score is not None:
                scores[image] = int(score[0])

        if descending:
            sorted_keys = sorted(scores, key=scores.get, reverse=True)
        else:
            sorted_keys = sorted(scores, key=scores.get, reverse=False)

        return {"sorted_ids": sorted_keys}
