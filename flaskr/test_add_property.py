#import pytest
# import unittest
#from app import create_app
#import db
#from flask import json

# from add_property import expected_elo

# https://medium.com/analytics-vidhya/how-to-test-flask-applications-aef12ae5181c#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc3NDU3MzIxOGM2ZjZhMmZlNTBlMjlhY2JjNjg2NDMyODYzZmM5YzMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJuYmYiOjE2MTgzMDA3NzIsImF1ZCI6IjIxNjI5NjAzNTgzNC1rMWs2cWUwNjBzMnRwMmEyamFtNGxqZGNtczAwc3R0Zy5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwMjgyMTA5MTcxNTE0MDUxMTEwMSIsImVtYWlsIjoiYW5kcmVhc3dAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF6cCI6IjIxNjI5NjAzNTgzNC1rMWs2cWUwNjBzMnRwMmEyamFtNGxqZGNtczAwc3R0Zy5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsIm5hbWUiOiJBbmRyZWFzIFfDpWfDuCBXaWxzb24iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1vVzYxeHdvZWNzSS9BQUFBQUFBQUFBSS9BQUFBQUFBQUpsSS9BTVp1dWNuZkliakdpMEJPeTI5bjVOdGVrLUJlUVBWX0NBL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJBbmRyZWFzIFfDpWfDuCIsImZhbWlseV9uYW1lIjoiV2lsc29uIiwiaWF0IjoxNjE4MzAxMDcyLCJleHAiOjE2MTgzMDQ2NzIsImp0aSI6IjVlODQ2ZjE1MzY1YmYwODQ0OGIzMzkxNzY5MTI1MzU2YjViYzdhYWMifQ.oHdhFfkvibM8bvFnv2VYdDDU_02e0Nqv69kXNx2ZLtxNRAdKRv-cWxuP5GmntmDlTsJE8nvEMzHZIlAcZlTyo9HhKuhhuYlSQF0qeUNWyaBgeTX6ZVNLwcuQiH5Abj53Dyp0jwsofEEqD7DK2w-YIFrPRBt_6pUZfeIS0RAXokLHjq3e9LmxIvOfoeGBOdavOB-O0ExBCo-ZRL2NpGiA2VxeVoO92qoNbwnrvZuObl4XDS72IyYlWVIpResp9w4iMl9RqXGo6KhsPRqFrR8liBR0eoetlvt8qMN-Xsp-vQxaQNKVyI_2bmiXtZwsTQp8rs3aXK4LiLic60WI7g-snQ

"""
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.testing = True
    # This creates an in-memory sqlite db
    # See https://martin-thoma.com/sql-connection-strings/
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    client = app.test_client()
    with app.app_context():
        db.init_db()
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (?, ?, ?)",
            (1234, "elo", 1000),
        )
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (?, ?, ?)",
            (4321, "elo", 1000),
        )
        db.get_db().commit()
    yield client


def test_getting_data_from_db(client):
    rv = client.get("/props/properties?image=1234")
    data = json.loads(rv.get_data(as_text=True))
    assert data == {"1": {"category": "elo", "score": 1000}}

"""

# class TestExpectedEloScore(unittest.TestCase):

#     #Testing with a difference of 200 rating points. 
#     #The expected score should be 0.75, according to the elo rating system.
#     def test_elo_expected_score_calculation(self):
#         value = expected_elo(1500, 1300) 
#         self.assertEqual(value, 0.75)

#     #Testing if: expected_elo(A,B) + expected_elo(B,A) == 1
#     def test_elo_expected_score_calculation2(self):
#         acummulated = expected_elo(1500, 1300) + expected_elo(1300, 1500)
#         self.assertEqual(acummulated, 1)


# if __name__ == '__main__':
#     unittest.main()
