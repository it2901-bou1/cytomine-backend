import pytest
from app import create_app
import db
from flask import json
from unittest.mock import MagicMock
from pytest_mock import mocker


@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.testing = True
    # This creates an in-memory sqlite db
    # See https://martin-thoma.com/sql-connection-strings/
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    client = app.test_client()
    with app.app_context():
        db.init_db()
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (1, 'elo', 1000)",
        )
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (2, 'elo', 2000)",
        )
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (3, 'elo', 3000)",
        )
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (4, 'elo', 3000)",
        )
        db.get_db().commit()
    yield client


class Mock_image:
    def __init__(self, id):
        self.id = id


def test_get_images(client, mocker):
    mocker.patch(
        'cytomine.models.ImageInstanceCollection.fetch_with_filter'
    ).return_value = [Mock_image(1), Mock_image(2), Mock_image(3)]

    rv = client.get("/get_images/123")
    data = json.loads(rv.get_data(as_text=True))
    assert data is not None

    images = data["id_images"]
    assert len(images) == 3
    assert images[0] == 1

    rv = client.get("/get_images/123/1")
    data = json.loads(rv.get_data(as_text=True))

    images = data["id_images"]
    assert len(images) == 1


def test_get_two_elo_images(client, mocker):
    mocker.patch(
        'cytomine.models.ImageInstanceCollection.fetch_with_filter'
    ).return_value = [Mock_image(1), Mock_image(2), Mock_image(3), Mock_image(4)]

    rv = client.get("/get_two_elo_images/123")
    data = json.loads(rv.get_data(as_text=True))
    assert data is not None

    # Tests if unrated images are prioritized over those with same rating
    image1 = data["id_image1"]
    image2 = data["is_image_2"]
    assert len(data) == 2
    assert image1 == 1 or image2 == 1

    mocker.patch(
        'cytomine.models.ImageInstanceCollection.fetch_with_filter'
    ).return_value = [Mock_image(2), Mock_image(3), Mock_image(4)]

    rv = client.get("/get_two_elo_images/123")
    data = json.loads(rv.get_data(as_text=True))
    image1 = data["id_image1"]
    image2 = data["id_image2"]

    # Tests if same rated images prioritized
    assert len(data) == 2
    assert image1 == 3
    assert image2 == 4


def test_get_two_random(client, mocker):
    mocker.patch(
        'cytomine.models.ImageInstanceCollection.fetch_with_filter'
    ).return_value = [Mock_image(1), Mock_image(2), Mock_image(3), Mock_image(4)]

    rv = client.get("/get_two_random_images/123")
    data = json.loads(rv.get_data(as_text=True))
    image1 = data["id_image1"]
    image2 = data["id_image2"]
    assert image1 != image2
    assert image1 in [1, 2, 3, 4]
    assert image2 in [1, 2, 3, 4]
