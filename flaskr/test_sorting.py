import pytest
from app import create_app
import db
from flask import json
from unittest.mock import MagicMock
from pytest_mock import mocker


@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.testing = True
    # This creates an in-memory sqlite db
    # See https://martin-thoma.com/sql-connection-strings/
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    client = app.test_client()
    with app.app_context():
        db.init_db()
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (1, 'elo', 1000)",
        )
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (2, 'elo', 2000)",
        )
        db.get_db().execute(
            "INSERT INTO rating (imageid, category, score) VALUES (3, 'elo', 3000)",
        )
        db.get_db().commit()
    yield client


class Mock_image:
    def __init__(self, id):
        self.id = id


def test_sorting_elo(client, mocker):
    mocker.patch(
        'cytomine.models.ImageInstanceCollection.fetch_with_filter'
    ).return_value = [Mock_image(1), Mock_image(2), Mock_image(3)]

    rv = client.get("/sort/elo?project=123")

    data = json.loads(rv.get_data(as_text=True))
    assert data is not None

    ordered_ids = data["sorted_ids"]
    assert ordered_ids[0] == 1
    assert ordered_ids[1] == 2
    assert ordered_ids[2] == 3
